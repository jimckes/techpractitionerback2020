# Imagen Docker base/inicial
FROM node:latest
# Crear directorio del contenedor Docker
WORKDIR /docker-api
# Copiar archivos del proyecto en dire#ctorio del contenedor
ADD . /docker-api
# Instalar dependencias de producción
# RUN npm install --only=production
# Exponer puerto escucha del contenedor (mismo que definimos en nuestra API)
EXPOSE 3000
# Lanzar comandos para ejecutar nuesta app
# CMD ["node", "server.js"]
CMD ["npm", "run", "pro"]

# (1) Crear imagen docker
# sudo docker build -t imagesbacktech:3.0 .

# (2) Ver imagenes creadas
# sudo docker images

# (3) Levantar y correr la imagen
# sudo docker run -p 3000:3000 imagesbacktech:3.0

# (4) Subir la imagen a repositorio docker
# sudo docker tag imagesbacktech:3.0 jimckes/imagesbacktech:3.0
# sudo docker login docker.io
# sudo docker push jimckes/imagesbacktech:3.0
