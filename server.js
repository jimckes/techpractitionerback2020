var express = require('express');//Referencia al paquete express, equivale a import
var app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());

const requestJSON = require('request-json');

var https = require('https');

require('dotenv').config();

const cors = require('cors');  // Instalar dependencia 'cors' con npm
var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "*");
 next();  // middleware para decirle que pase por aquí primero
}
app.use(enableCORS);

// npm install
// Iniciar servidor -> npm run dev

const baseMLabURL = 'https://api.mlab.com/api/1/databases/techu12db/collections/';
//const apikeyMLab = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';
const apikeyMLab = 'apiKey=' + process.env.MLAB_APIKEY;
const apikeyWeather = 'appid='+process.env.KEY_WEATHER;

var URL_BASE = '/api-peru/v1/';

// GET users a través de mLab
app.get(URL_BASE + 'usersMLAB',
  function(req, res) {
    const httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    const fieldParam = 'f={"_id":0}&';
    //const fieldParam = 'q={"first_name":"Jimy"}&';
    console.log('user?' + fieldParam+ apikeyMLab);
    httpClient.get('user?' + fieldParam+ apikeyMLab,
     function(err, respuestaMLab, body) {
       console.log('Error: ' + err);
       console.log('Respuesta MLab: ' + respuestaMLab);
       console.log('Body: ' + body);
       var response = {};
       if(err) {
           response = {"msg" : "Error al recuperar users de mLab."}
           res.status(500);
       } else {
         if(body.length > 0) {
           response = body;
         } else {
           response = {"msg" : "Usuario no encontrado."};
           res.status(404);
         }
       }
       res.send(response);
     }); //httpClient.get(
}); //app.get(

// GET user /:id
  app.get(URL_BASE + 'usersMLAB/:id',
   function (req, res) {
     console.log("GET /colapi/v3/users/:id");
     console.log(req.params.id);
     var id = req.params.id;
     var queryString = 'q={"id":' + id + '}&';
     var queryStrField = 'f={"_id":0}&';
     var httpClient = requestJSON.createClient(baseMLabURL);
     httpClient.get('user?' + queryString + queryStrField + apikeyMLab,
       function(err, respuestaMLab, body){
         console.log("Respuesta mLab correcta.");
       //  var respuesta = body[0];
         var response = {};
         if(err) {
             response = {"msg" : "Error obteniendo usuario."}
             res.status(500);
         } else {
           if(body.length > 0) {
             response = body[0];
           } else {
             response = {"msg" : "Usuario no encontrado."}
             res.status(404);
           }
         }
         res.send(response);
       });
 });

 //PUT of user /:id
 app.put(URL_BASE + 'usersMLAB/:id',
 function(req, res) {
 var  clienteMlab = requestJSON.createClient(baseMLabURL);
 console.log("req.params.id:" + req.params.id);
 console.log("req.body.id:" + req.body.email);
  clienteMlab.get('user?'+ apikeyMLab ,
  function(error, respuestaMLab , body) {

      newID=req.params.id;
      console.log("newID:" + newID);
      var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
      clienteMlab.put(baseMLabURL + 'user?q={"id": ' + newID + '}&' + apikeyMLab, JSON.parse(cambio),
       function(error, respuestaMLab, body) {
         console.log("body:"+ body);
        res.send(body);
      });
  });
 });

  //POST of user
 app.post(URL_BASE + "usersMLAB",
  function(req, res) {
   var  clienteMlab = requestJSON.createClient(baseMLabURL);
   clienteMlab.get('user?'+ apikeyMLab ,
   function(error, respuestaMLab , body) {
       newID=body.length+1;
       console.log("newID:" + newID);
       var newUser = {
         "id" : newID,
         "first_name" : req.body.first_name,
         "last_name" : req.body.last_name,
         "email" : req.body.email,
         "password" : req.body.password,
         "ciudad" : req.body.ciudad
       };
       clienteMlab.post(baseMLabURL + "user?" + apikeyMLab, newUser ,
        function(error, respuestaMLab, body) {
         res.send(body);
      });
   });
 });

 //DELETE user with /:id
app.delete(URL_BASE + "usersMLAB/:id",
  function(req, res){
    console.log("entra al DELETE");
    console.log("request.params.id: "+req.params.id);
    var id=req.params.id;
    var queryStringID='q={"id":' + id + '}&';
    console.log(baseMLabURL + 'user?' + queryStringID + apikeyMLab);
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' +  queryStringID + apikeyMLab,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        console.log("body delete:"+ respuesta);
        httpClient.delete(baseMLabURL + "user/"+ respuesta._id.$oid +'?'+ apikeyMLab,
          function(error, respuestaMLab,body){
            res.send(body);
        });
      });
  });

  //Get account with /:iduser
  app.get(URL_BASE + 'usersMLAB/:id/accounts',
   function (req, res) {
    console.log("request.params.id: " + req.params.id);
    var id = req.params.id;
    var queryStringID = 'q={"userID":' + id + '}&';
    var queryString = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('account?' + queryString + queryStringID + apikeyMLab,
      function(error, respuestaMLab, body){
        console.log('error '+ error);
        console.log('respuestaMLab ' + respuestaMLab);
        console.log('body ' + body);
        var respuesta = body;
        //var respuesta = {};
        respuesta = !error ? body : {"msg":"usuario con ese ID no encontrado"};
        res.send(respuesta);
      });
  });


  //Method GET with params MLab users and movements with IDAccount
  app.get(URL_BASE + "usersMLAB/account/:ida/movement",
  function (req, res) {
    console.log("GET/colapi/v3/users/account/:id/movement");
    var ida=req.params.ida;
    var queryStringIDA='q={"accountID":' + ida + '}&';
    var queryString = 'f={"_id":0}&';//s={1}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('movement?' + queryString +  queryStringIDA + apikeyMLab,
      function(error, respuestaMLab, body){
        console.log(baseMLabURL +'movement?'+ queryString+queryStringIDA+apikeyMLab);
        //var respuesta = body;movimientos
        var respuesta = {};
        respuesta = !error ? body   : {"msg":"usuario con ese ID no encontrado"};

        var result = [];
        var balance=sumatoriaSaldo(body);
        body.sort(function(a, b){return a.movID - b.movID});

        result.push({movimientos: body, saldo: balance});

        var bodySaldo = {
          "balance": "S/ "+balance
        };
        var cambio = '{"$set":' + JSON.stringify(bodySaldo) + '}';
        httpClient.put('account?q={"accountID": ' + parseInt(ida) + '}&' + apikeyMLab, JSON.parse(cambio),
         function(error, respuestaMLab, body1) {
           console.log("balance actualizad:"+ cambio);
           respuesta = !error ? body1   : {"msg":"balance no actualizado"};
        });
        res.contentType('application/json');
        res.send(JSON.stringify(result));

        /*res.jsonp({
          saldo: "50",
          movimientos: body,
          revision: "1"
        });*/

      });
  });

  //POST of movement with :idacoount
  app.post(URL_BASE + "usersMLAB/account/:ida/movement",
   function(req, res) {
    var  clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('movement?'+ apikeyMLab ,
    function(error, respuestaMLab , body) {
        newID=body.length+1;
        console.log("newID:" + newID);
        var newMov = {
          "movID" : newID,
                "accountID": parseInt(req.params.ida),
                "import": req.body.import,
                "date": req.body.date,
                "descripcion": req.body.descripcion,
                "Type": req.body.Type,
                "centro": 333
            }
        clienteMlab.post(baseMLabURL + "movement?" + apikeyMLab, newMov ,
         function(error, respuestaMLab, body) {
          res.send(body);
       });

    });
  });

  //POST create transfer
  app.post(URL_BASE + "usersMLAB/transfer",
  function(req, res) {
   var  clienteMlab = requestJSON.createClient(baseMLabURL);
   clienteMlab.get('movement?'+ apikeyMLab ,
   function(error, respuestaMLab , body) {
       newIDSender=body.length+1;
       newIDreceiver=body.length+2;
       var iban= req.body.receiver.account.iban;
       console.log("newIDSender "+newIDSender);
       console.log("newIDreceiver "+newIDreceiver);
       console.log("iban "+iban);
       var queryStringIDA='q={"iban":"'+iban+'"}&';
       var queryString = 'f={"_id":0}&';
       //var httpClient = requestJSON.createClient(baseMLabURL);
       clienteMlab.get('account?' + queryString +  queryStringIDA + apikeyMLab,
         function(error, respuestaMLab, bodyAccountReceiver){
           //console.log("url "+baseMLabURL+ 'account?' + queryString +  queryStringIDA );
           console.log("bodyAccountReceiver "+ bodyAccountReceiver[0].accountID );
           //console.log("newID:" + newID);
           var newMovSender = {
                   "movID" : newIDSender,
                   "accountID": req.body.sender.account.id,
                   "import": -req.body.import,
                   "date": req.body.date,
                   "descripcion": req.body.description,
                   "Type": req.body.Type,
                   "centro": 814
               }
            var newMovReceiver = {
                   "movID" : newIDreceiver,
                    "accountID": bodyAccountReceiver[0].accountID,
                    "import": req.body.import,
                       "date": req.body.date,
                       "descripcion": req.body.description,
                       "Type": req.body.Type,
                       "centro": 814
            }
            console.log("newMovSender "+newMovSender);
            console.log("newMovReceiver "+newMovReceiver);
            //res.send(bodyAccountReceiver);
           clienteMlab.post(baseMLabURL + "movement?" + apikeyMLab, newMovSender ,
            function(error, respuestaMLab, bodyS) {
              respuesta = !error ? bodyS   : {"msg":"newMovSender no actualizado"};
              console.log(respuesta);
              clienteMlab.post(baseMLabURL + "movement?" + apikeyMLab, newMovReceiver ,
               function(error, respuestaMLab, bodyR) {
                 respuesta = !error ? bodyR   : {"msg":"newMovReceiver no actualizado"};
                 res.send(respuesta);
              });
           });

       });

   });
  });

function sumatoriaSaldo(mov) {
    var sumatoria = 0;
    for ( var i = 0; i < mov.length; i++ ) {
        //if ( mov[i] > 0 ) {
        console.log(mov[i].import);
        sumatoria=sumatoria+mov[i].import;
      //  }
    }
    return sumatoria.toFixed(2);
}

  //API EXTERNO - get openweathermap
  const baseWeatherURL = 'https://api.openweathermap.org/data/2.5/weather';
  //Method GET with params MLab users and movements with ID
  app.get(URL_BASE + "clima/:idCiudad",
  function (req, res) {
    console.log("api.openweathermap.org");
    var ida=req.params.idCiudad;
    var queryStringIDA='q=' + ida + ',pe}&';
    var httpClient = requestJSON.createClient(baseWeatherURL);
    httpClient.get('?' +queryStringIDA + apikeyWeather,
      function(error, respuestaMLab, body){
        console.log(baseWeatherURL +'?' +queryStringIDA + apikeyWeather);
        //var respuesta = body;
        var respuesta = {};
        respuesta = !error ? body   : {"msg":"clima ID no encontrado"};
        res.send(respuesta);
      });
  });


//Method POST login
app.post(URL_BASE + "usersMLAB/login",
  function (req, res){
    console.log("POST /colapi/v3/login");
    console.log(req.body);
    var email= req.body.email;
    var pass= req.body.password;
    var queryStringEmail='q={"email":"' + email + '"}&';
    var queryStringpass='q={"password":' + pass + '}&';
    var  clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?'+ queryStringEmail+apikeyMLab ,
    function(error, respuestaMLab , body) {
      console.log("entro al body:" + body );
      var respuesta = body[0];
      console.log(respuesta);
      if(respuesta!=undefined){
          if (respuesta.password == pass) {
            console.log("Login Correcto");
            var session={"logged":true};
            var login = '{"$set":' + JSON.stringify(session) + '}';
            //console.log(baseMLabURL+'?q={"id": ' + respuesta.id + '}&' + apikeyMLab);
            clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apikeyMLab, JSON.parse(login),
             function(errorP, respuestaMLabP, bodyP) {
              res.send(body[0]);
            });
          }
          else {
            res.send({"msg":"contraseña incorrecta"});
          }
      }else{
        console.log("Email Incorrecto");
        res.send({"msg": "email Incorrecto"});
      }
    });
});

//Method POST logout
app.post(URL_BASE + "usersMLAB/logout",
  function (req, res){
    console.log("POST /colapi/v3/logout");
    var email= req.body.email;
    var queryStringEmail='q={"email":"' + email + '"}&';
    var  clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?'+ queryStringEmail+apikeyMLab ,
    function(error, respuestaMLab , body) {
      console.log("entro al get");
      var respuesta = body[0];
      console.log(respuesta);
      if(respuesta!=undefined){
            console.log("logout Correcto");
            var session={"logged":true};
            var logout = '{"$unset":' + JSON.stringify(session) + '}';
            console.log(logout);
            clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apikeyMLab, JSON.parse(logout),
             function(errorP, respuestaMLabP, bodyP) {
              res.send(body[0]);
              //res.send({"msg": "logout Exitoso"});
            });
      }else{
        console.log("Error en logout");
        res.send({"msg": "Error en logout"});
      }
    });
});



//const PORT = 3000;
const PORT = process.env.PORT || 3000;
app.listen(3000,function(){
  console.log('Node JS escuchando puerto 3000')
});

const PORT_HTTPS = 443;
var userFile = require('./user.json');
var fs = require('fs');
/*
https.createServer({
    key: fs.readFileSync('my_cert.key'),
    cert: fs.readFileSync('my_cert.crt')
}, app).listen(PORT, function(){
    console.log("My https server listening on port " + PORT_HTTPS + "...");
});*/
