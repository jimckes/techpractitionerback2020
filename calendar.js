/ **
* @license
 * Copyright Google Inc.
 * *
 * Licenciado bajo la Licencia Apache, Versión 2.0 (la "Licencia");
 * no puede usar este archivo excepto en cumplimiento de la Licencia.
 * Puede obtener una copia de la Licencia en
 * *
 * https://www.apache.org/licenses/LICENSE-2.0
 * *
 * A menos que lo requiera la ley aplicable o se acuerde por escrito, el software
 * distribuido bajo la Licencia se distribuye "TAL CUAL",
 * SIN GARANTÍAS O CONDICIONES DE NINGÚN TIPO, ya sea expresa o implícita.
 * Consulte la Licencia para ver los permisos específicos de idioma y
 * limitaciones bajo la Licencia.
 * /
// [START calendar_quickstart]
const  fs  =  require ( 'fs' ) ;
const  readline  =  require ( 'readline' ) ;
const  { google }  =  require ( 'googleapis' ) ;

// Si modifica estos ámbitos, elimine token.json.
const  SCOPES  =  [ 'https://www.googleapis.com/auth/calendar.readonly' ] ;
// El archivo token.json almacena los tokens de acceso y actualización del usuario, y es
// creado automáticamente cuando se completa el flujo de autorización para el primer
// hora.
const  TOKEN_PATH  =  'token.json' ;

// Cargar secretos de clientes desde un archivo local.
fs . readFile ( 'credentials.json' ,  ( err ,  content )  =>  {
  if  ( err )  devuelve la  consola . log ( 'Error al cargar el archivo secreto del cliente:' ,  err ) ;
  // Autorice a un cliente con credenciales, luego llame a la API de Google Calendar.
  autorizar ( JSON . parse ( contenido ) ,  listEvents ) ;
} ) ;

/ **
 * Cree un cliente OAuth2 con las credenciales dadas y luego ejecute el
 * dada la función de devolución de llamada.
* @param { Object } credenciales Las credenciales del cliente de autorización.
* @param { function } callback La devolución de llamada para llamar con el cliente autorizado.
 * /
función  autorizar ( credenciales ,  devolución de llamada )  {
  const  { client_secret , client_id , redirect_uris }  =  credenciales . instalado ;
  const  oAuth2Client  =  nuevo  google . auth . OAuth2 (
      client_id ,  client_secret ,  redirect_uris [ 0 ] ) ;

  // Comprueba si previamente hemos almacenado un token.
  fs . readFile ( TOKEN_PATH ,  ( err ,  token )  =>  {
    if  ( err )  devuelve  getAccessToken ( oAuth2Client ,  callback ) ;
    oAuth2Client . setCredentials ( JSON . parse ( token ) ) ;
    devolución de llamada ( oAuth2Client ) ;
  } ) ;
}

/ **
 * Obtenga y almacene un nuevo token después de solicitar la autorización del usuario, y luego
 * ejecuta la devolución de llamada dada con el cliente OAuth2 autorizado.
* @param { google.auth.OAuth2 } oAuth2Client El cliente OAuth2 para obtener el token.
* @param { getEventsCallback } devolución de llamada La devolución de llamada para el cliente autorizado.
 * /
función  getAccessToken ( oAuth2Client ,  callback )  {
  const  authUrl  =  oAuth2Client . generateAuthUrl ( {
    tipo_acceso : 'fuera de línea' ,
    alcance : ALCANCE ,
  } ) ;
  consola . log ( 'Autorice esta aplicación visitando esta url:' ,  authUrl ) ;
  const  rl  =  readline . createInterface ( {
    entrada : proceso . stdin ,
    salida : proceso . stdout ,
  } ) ;
  rl . pregunta ( 'Ingrese el código de esa página aquí:' ,  ( código )  =>  {
    rl . cerrar ( ) ;
    oAuth2Client . getToken ( code ,  ( err ,  token )  =>  {
      if  ( err )  devuelve la  consola . error ( 'Error al recuperar el token de acceso' ,  err ) ;
      oAuth2Client . setCredentials ( token ) ;
      // Almacenar el token en el disco para posteriores ejecuciones del programa
      fs . writeFile ( TOKEN_PATH ,  JSON . stringify ( token ) ,  ( err )  =>  {
        if  ( err )  devuelve la  consola . error ( err ) ;
        consola . log ( 'Token almacenado en' ,  TOKEN_PATH ) ;
      } ) ;
      devolución de llamada ( oAuth2Client ) ;
    } ) ;
  } ) ;
}

/ **
 * Enumera los próximos 10 eventos en el calendario principal del usuario.
* @param { google.auth.OAuth2 } auth Un cliente OAuth2 autorizado.
 * /
función  listEvents ( auth )  {
   calendario  const =  google . calendario ( { versión : 'v3' , auth } ) ;
  calendario . eventos . lista ( {
    calendarId : 'primario' ,
    timeMin : ( nueva  fecha ( ) ) . toISOString ( ) ,
    maxResults : 10 ,
    singleEvents : verdadero ,
    orderBy : 'startTime' ,
  } ,  ( err ,  res )  =>  {
    if  ( err )  devuelve la  consola . log ( 'La API devolvió un error:'  +  err ) ;
     eventos  const =  res . los datos . artículos ;
    if  ( eventos . duración )  {
      consola . log ( 'Próximos 10 eventos:' ) ;
      eventos . mapa ( ( evento ,  i )  =>  {
        const  start  =  evento . comenzar . dateTime  ||  evento . comenzar . fecha ;
        consola . log ( ` $ { start } - $ { event . summary } ` ) ;
      } ) ;
    }  más  {
      consola . log ( 'No se encontraron eventos próximos' ) ;
    }
  } ) ;
}
// [END calendar_quickstart]

módulo . exportaciones  =  {
  ALCANCES ,
  listEvents ,
} ;
