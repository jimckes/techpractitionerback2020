//operacion post a users
app.post(URL_BASE + 'users',
 function(req, res){
   console.log(req.body);
   console.log(req.body.first_name);
   let newUser = {
     id: req.body.id,
     first_name:req.body.first_name,
     last_name: req.body.last_name,
     email: req.body.email,
     password: req.body.password
   }
   userFile.push(newUser);
   res.status(201);
   res.send({"Nombre":"Usuario creado correctamente",
           "usuario": newUser,
           "userFile actualizado": userFile[req.body.id]});
 });


//operacion  put
 app.put(URL_BASE + 'users/:id',
 function(req, res){
 let aux = parseInt(req.params.id);
   console.log(req.body);
   console.log(req.body.first_name);
   let newUser = {
     id: req.body.id,
     first_name:req.body.first_name,
     last_name: req.body.last_name,
     email: req.body.email,
     password: req.body.password
   }

   for (i = 0; i < userFile.length; i++){
     if(userFile[i].id == aux) {
         userFile[i] = newUser;
     }
   }

   res.status(201);
   res.send({"Nombre":"Usuario actualizado correctamente",
           "usuario": newUser,
           "userFile actualizado": userFile[req.body.id]});
 });

 //operacion delete
 // Delete
app.delete(URL_BASE + 'users/:id',
    function(req, res){
    let indice = parseInt(req.params.id);
    //userFile.splice(indice -1,1);
    let respuesta = (userFile[indice-1] == undefined) ?
    {"msg": "No existe"} :
    {"Usuario Eliminado" : (userFile.splice(indice-1,1))}
    res.status(201).send(respuesta);
  });

  // LOGIN - user.json
 app.post(URL_BASE + 'login',
  function(request, response) {
  // console.log("POST /apitechu/v1/login");
   console.log(request.body);
   console.log(request.body.password);
   var user = request.body.email;
   var pass = request.body.password;
   for(us of userFile) {
   if(us.email == user) {
    if(us.password == pass) {
     us.logged = true;
     writeUserDataToFile(userFile);
     console.log("Login correcto!");
     response.send({"msg" : "Login correcto.",
             "idUsuario" : us.id,
              "logged" : "true"});
    } else {
     console.log("Login incorrecto.");
     response.send({"msg" : "Login incorrecto."});
    }
   }
  }
});

// LOGOUT - users.json
app.post(URL_BASE + 'logout',
 function(request, response) {
  //console.log("POST /apitechu/v1/logout");
  var userId = request.body.id;
  for(us of userFile) {
   if(us.userID == userId) {
    if(us.logged) {
      delete us.logged; // borramos propiedad 'logged'
      writeUserDataToFile(userFile);
      console.log("Logout correcto!");
      response.send({"msg" : "Logout correcto.", "idUsuario" : us.id});
      } else {
      console.log("Logout incorrecto.");
      response.send({"msg" : "Logout incorrecto."});
      }
    }
  }
});

function writeUserDataToFile(data) {
 var fs = require('fs');
 var jsonUserData = JSON.stringify(data);
 fs.writeFile("./user.json", jsonUserData, "utf8",
  function(err) { //función manejadora para gestionar errores de escritura
   if(err) {
    console.log(err);
   } else {
    console.log("Datos escritos en 'user.json'.");
   }
  })
}
